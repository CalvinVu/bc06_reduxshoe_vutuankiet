import { data } from "../Component/Data";

let initialState = {
  data: data,
  detail: data[0],
  cart: [],
};
export let numberRedux = (state = initialState, action) => {
  switch (action.type) {
    case "DETAIL": {
      state.detail = action.payload;
      return { ...state };
    }
    case "ADD": {
      let postCart = [...state.cart];
      let index = postCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        postCart.push(action.payload);
        action.payload.soLuong = 1;
      } else {
        postCart[index].soLuong++;
      }
      return { ...state, cart: postCart };
    }
    case "COUNT": {
      let postCart = [...state.cart];
      let index = postCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      postCart[index].soLuong = postCart[index].soLuong + action.number;
      postCart[index].soLuong == 0 && postCart.splice(index, 1);
      return { ...state, cart: postCart };
    }
    case "DELETE": {
      let postCart = [...state.cart];
      let index = postCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      postCart.splice(index, 1);
      return { ...state, cart: postCart };
    }
    default: {
      return state;
    }
  }
};
