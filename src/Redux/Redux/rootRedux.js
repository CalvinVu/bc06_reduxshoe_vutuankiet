import { combineReducers } from "redux";
import { numberRedux } from "./numberRedux";

export let rootRedux = combineReducers({ numberRedux });
