import React, { Component } from "react";
import { connect } from "react-redux";

class ExRedux extends Component {
  renderListShoe = () => {
    return this.props.data.map((item, index) => {
      return (
        <div className="card col-4" style={{ width: "18rem" }}>
          <p>{index}</p>
          <img className="card-img-top" src={item.image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title" style={{ fontSize: "15px" }}>
              {item.name}
            </h5>
            <p className="card-text">{item.price}</p>
            <button
              className="btn btn-primary"
              onClick={() => {
                this.props.handleAdd(item);
              }}
            >
              ADD
            </button>
            <button
              className="btn btn-success"
              onClick={() => {
                this.props.handleDetail(item);
              }}
            >
              DETAIL
            </button>
          </div>
        </div>
      );
    });
  };
  renderDetail = () => {
    return (
      <div className="card" style={{ width: "14rem" }}>
        <img
          className="card-img-top"
          src={this.props.detail.image}
          alt="Card image cap"
        />
        <div className="card-body">
          <h5 className="card-title">{this.props.detail.name}</h5>
          <p className="card-text">
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </p>
          <a href="#" className="btn btn-primary">
            Go somewhere
          </a>
        </div>
      </div>
    );
  };
  renderTable = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.name}</td>
          <td>
            <img src={item.image} style={{ width: "50px" }}></img>
          </td>
          <td>
            <button
              className="btn btn-warning"
              onClick={() => {
                this.props.handleCount(item, -1);
              }}
            >
              -
            </button>
            {item.soLuong}
            <button
              className="btn btn-success"
              onClick={() => {
                this.props.handleCount(item, +1);
              }}
            >
              +
            </button>
          </td>
          <td>
            <button
              className="btn btn-dark"
              onClick={() => {
                this.props.handleDelete(item);
              }}
            >
              DELETE
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="row">
        <div className="row col-4">{this.renderListShoe()}</div>
        <div className="col-3">{this.renderDetail()}</div>
        <div className="col-5">
          <table className="table">
            <thead>
              <tr>
                <th>NAME</th>
                <th>IMAGE</th>
                <th>QUANTITY</th>
                <th>ACTION</th>
              </tr>
            </thead>
            <tbody>{this.renderTable()}</tbody>
          </table>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    data: state.numberRedux.data,
    detail: state.numberRedux.detail,
    cart: state.numberRedux.cart,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleDetail: (item) => {
      let action = {
        type: "DETAIL",
        payload: item,
      };
      dispatch(action);
    },
    handleAdd: (item) => {
      let action = {
        type: "ADD",
        payload: item,
      };
      dispatch(action);
    },
    handleCount: (item, number) => {
      let action = {
        type: "COUNT",
        payload: item,
        number,
      };
      dispatch(action);
    },
    handleDelete: (item) => {
      let action = {
        type: "DELETE",
        payload: item,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ExRedux);
